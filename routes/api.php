<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// private routes
Route::middleware('auth:api')->group(function () {
    Route::get('user', function (Request $request) {
        return $request->user();
    });
    Route::get('attackRobots', 'RobotController@attackRobots');
    Route::get('logout', 'Api\AuthController@logout');
    Route::get('robots/user', 'RobotController@userRobots');
    Route::resource('robots', 'RobotController');
    Route::resource('battles', 'BattleController');
});

// public routes
Route::get('defendRobots', 'RobotController@defendRobots');
Route::get('leaderboard', 'RobotController@leaderboard');
Route::get('recent', 'BattleController@recentBattles');
Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');
