# Robots Battle

## Setup Laravel Docker
### Reference
- https://medium.com/@shakyShane/laravel-docker-part-1-setup-for-development-e3daaefaf3c
- https://medium.com/@shakyShane/laravel-docker-part-2-preparing-for-production-9c6a024e9797
- https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose

### Steps
```sh
# Copy env
$ cp .env.example .env
# Start the containers
$ docker-compose up -d
# Install packages
$ docker-compose exec backend composer install
# Generate key
$ docker-compose exec backend php artisan key:generate
# Migrations
$ docker-compose exec backend php artisan migrate
# Seeds
$ docker-compose exec backend php artisan db:seed
# Passport
$ docker-compose exec backend php artisan passport:client --personal

# Useful commands
$ docker-compose exec backend php artisan optimize
$ docker-compose exec backend vendor/bin/phpunit
$ docker-compose exec backend npm run watch
```
