<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Robot extends Model
{
    protected $fillable = [
        'name',
        'weight',
        'power',
        'speed',
        'wins',
        'losses',
        'user_id'
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'wins' => 0,
        'losses' => 0,
    ];

    public function attacks()
    {
        return $this->hasMany(Battle::class, 'attacker_id');
    }

    public function defends()
    {
        return $this->hasMany(Battle::class, 'defender_id');
    }

    public function wins()
    {
        return $this->hasMany(Battle::class, 'winner');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
