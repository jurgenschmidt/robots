<?php

namespace App\Http\Controllers;

use App\Robot;
use App\Http\Resources\RobotResource;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class RobotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Robot::all();
    }

    /**
     * Returns the robots able to attack.
     *
     * @return \Illuminate\Http\Response
     */
    public function attackRobots(Request $request)
    {
        return Robot::where('user_id', $request->user()->id)
            ->whereHas('attacks', function ($query) {
                $query->whereDate('created_at', Carbon::today());
            }, '<', 5)->get();
    }

    /**
     * Returns the robots able to defend.
     *
     * @return \Illuminate\Http\Response
     */
    public function defendRobots()
    {
        return Robot::whereHas('defends', function ($query) {
            $query->whereDate('created_at', Carbon::today());
        }, '<', 1)->get();
    }

    /**
     * Returns the robots belonging to user.
     *
     * @return \Illuminate\Http\Response
     */
    public function userRobots(Request $request)
    {
        return Robot::where('user_id', $request->user()->id)->get();
    }

    /**
     * Display the top 10 robots.
     *
     * @return \Illuminate\Http\Response
     */
    public function leaderboard()
    {
        return Robot::orderBy('wins', 'desc')->take(10)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:robots|max:50',
            'weight' => 'required|numeric|min:1|max:10',
            'power' => 'required|numeric|min:1|max:10',
            'speed' => 'required|numeric|min:1|max:10'
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }

        $robot = Robot::create([
            'name' => $request->name,
            'weight' => $request->weight,
            'power' => $request->power,
            'speed' => $request->speed,
            'user_id' => $request->user()->id
        ]);

        return new RobotResource($robot);
    }

    /**
     * Display the specified resource.
     *
     * @param  Robot  $robot
     * @return \Illuminate\Http\Response
     */
    public function show(Robot $robot)
    {
        return $robot;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Robot  $robot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Robot $robot)
    {
        if ($request->user()->id !== $robot->user_id) {
            return response()->json(['errors' => 'You do not own this robot.'], 403);
        }

        $validator = Validator::make($request->all(), [
            'name' => "required|unique:robots,name,$request->id|max:50",
            'weight' => 'required|numeric|min:1|max:10',
            'power' => 'required|numeric|min:1|max:10',
            'speed' => 'required|numeric|min:1|max:10'
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }

        $robot->update($request->only(['name', 'weight', 'power', 'speed']));

        return new RobotResource($robot);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Robot  $robot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Robot $robot)
    {
        if ($request->user()->id !== $robot->user_id) {
            return response()->json(['errors' => 'You do not own this robot.'], 403);
        }

        $robot->delete();

        return response()->json(null, 204);
    }
}
