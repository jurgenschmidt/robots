<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Battle;
use App\Robot;
use App\Http\Resources\BattleResource;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BattleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Battle::all();
    }

    public function recentBattles()
    {
        return Battle::orderBy('created_at', 'desc')
            ->take(5)
            ->with(['attacker:id,name', 'defender:id,name', 'winner:id,name'])
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check if user owns attacking robot
        $owner = Robot::where('id', $request->attacker_id)->firstOrFail();
        if ($request->user()->id !== $owner->user_id) {
            return response()->json(['error' => 'You do not own this robot.'], 403);
        }

        // Check if attacking robot has less than 5 fights today
        try {
            Robot::where('id', $request->attacker_id)->whereHas('attacks', function ($query) {
                $query->whereDate('created_at', Carbon::today());
            }, '<', 5)->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            return response()->json(['error' => 'Robot has already fought enough today.'], 403);
        }

        // Check if defending robot has fought today
        try {
            Robot::where('id', $request->defender_id)->whereHas('defends', function ($query) {
                $query->whereDate('created_at', Carbon::today());
            }, '<', 1)->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            return response()->json(['error' => 'Robot has already been attacked today.'], 403);
        }

        // Set Attacker and Defender
        $attacker = Robot::where('id', $request->attacker_id)->first();
        $defender = Robot::where('id', $request->defender_id)->first();
        $atk_hp = $attacker->weight;
        $def_hp = $defender->weight;

        $defHit = $defender->power * $defender->speed - $defender->weight;
        $defDodge = $attacker->speed * 10 / $attacker->weight + $attacker->power;
        $defGoal = $defHit - $defDodge;
        $atkHit = $attacker->power * $attacker->speed - $attacker->weight;
        $atkDodge = $defender->speed * 10 / $defender->weight + $defender->power;
        $atkGoal = $atkHit - $atkDodge;

        while ($atk_hp > 0 && $def_hp > 0) {
            $random = rand(-10, 10);
            // defender attacks
            if ($random > $defGoal) {
                $atk_hp--;
            }
            // attacker attacks
            if ($random > $atkGoal) {
                $def_hp--;
            }
        }

        if ($atk_hp = 0) {
            $winner = $request->defender_id;
        } else {
            $winner = $request->attacker_id;
        }

        $battle = Battle::create([
            'attacker_id' => $request->attacker_id,
            'defender_id' => $request->defender_id,
            'winner' => $winner
        ]);

        $attacker = Robot::where('id', $request->attacker_id)->increment('losses');
        $defender = Robot::where('id', $request->defender_id)->increment('wins');

        return new BattleResource($battle);
    }

    /**
     * Display the specified resource.
     *
     * @param  Battle  $battle
     * @return \Illuminate\Http\Response
     */
    public function show(Battle $battle)
    {
        return new BattleResource($battle);
    }
}
