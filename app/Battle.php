<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Battle extends Model
{
    protected $fillable = ['attacker_id', 'defender_id', 'winner'];

    public function attacker()
    {
        return $this->belongsTo(Robot::class, 'attacker_id');
    }

    public function defender()
    {
        return $this->belongsTo(Robot::class, 'defender_id');
    }

    public function winner()
    {
        return $this->belongsTo(Robot::class, 'winner');
    }
}
