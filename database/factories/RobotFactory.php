<?php

use Faker\Generator as Faker;

$factory->define(App\Robot::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'weight' => $faker->randomDigitNotNull,
        'power' => $faker->randomDigitNotNull,
        'speed' => $faker->randomDigitNotNull
    ];
});
