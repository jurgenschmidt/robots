<?php

use Illuminate\Database\Seeder;

class RobotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 5 Users
        factory(App\User::class, 5)->create()->each(function ($user) {
            // Create 3 Robots
            $robots = factory(App\Robot::class, 3)->make();
            $user->robots()->saveMany($robots);

            $robots->each(function ($robot) {
                // Make 1 to 3 battles for each robot
                $battles = factory(App\Battle::class, rand(1, 3))->make([
                    'attacker_id' => $robot->id
                ]);

                $battles->each(function ($battle) {
                    // Random pick a Robot to lose to
                    $defender = App\Robot::all()->random();
                    $defender->increment('wins');

                    $battle->defender_id = $defender->id;
                    $battle->winner = $defender->id;
                    $battle->save();
                });

                // Make the lose count
                $robot->increment('losses');
            });
        });

    }
}
