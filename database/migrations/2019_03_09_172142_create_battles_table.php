<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('attacker_id');
            $table->foreign('attacker_id')->references('id')->on('robots')->onDelete('cascade');
            $table->unsignedBigInteger('defender_id');
            $table->foreign('defender_id')->references('id')->on('robots')->onDelete('cascade');
            $table->unsignedBigInteger('winner');
            $table->foreign('winner')->references('id')->on('robots')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battles');
    }
}
