import React from 'react'
import Cookies from 'js-cookie'
import {
  Container,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
  Button
} from 'reactstrap'

import { withContext } from '../context/AppProvider'

class Fight extends React.Component {
  constructor() {
    super()
    this.state = {
      attackers: [],
      defenders: [],
      data: {},
      loading: true,
      battle: false
    }
  }

  componentDidMount() {
    axios
      .get('/api/attackRobots', {
        headers: {
          Authorization: 'Bearer ' + Cookies.get('token')
        }
      })
      .then(response => {
        this.setState({ attackers: response.data })
      })

    axios.get('/api/defendRobots').then(response => {
      this.setState({ defenders: response.data, loading: false })
    })
  }

  onChange(propertyName, event) {
    const { data } = this.state
    data[propertyName] = event.target.value
    this.setState({ data })
  }

  onSubmit() {
    const { history } = this.props
    const { data } = this.state

    this.setState({ battle: true })

    axios
      .post('/api/battles', data, {
        headers: {
          Authorization: 'Bearer ' + Cookies.get('token')
        }
      })
      .then(() => {
        history.push('/')
      })
      .catch(error => {
        this.setState({ loading: false })
        console.log('err', error)
      })
  }

  render() {
    const { attackers, defenders, loading, battle } = this.state
    const {
      context: { isAuthenticated }
    } = this.props

    if (!isAuthenticated) return <h1>You're not Authenticated.</h1>

    return (
      <Container>
        {loading ? (
          <h3>Loading...</h3>
        ) : (
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label>Your Robot</Label>
                <Input
                  type="select"
                  name="attacker_id"
                  onChange={this.onChange.bind(this, 'attacker_id')}
                >
                  <option value="">---</option>
                  {attackers.map(attacker => (
                    <option value={attacker.id} key={attacker.id}>
                      {attacker.name}
                    </option>
                  ))}
                </Input>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label>Your Oponent</Label>
                <Input
                  type="select"
                  name="defender_id"
                  onChange={this.onChange.bind(this, 'defender_id')}
                >
                  <option value="">---</option>
                  {defenders.map(defender => (
                    <option value={defender.id} key={defender.id}>
                      {defender.name}
                    </option>
                  ))}
                </Input>
              </FormGroup>
            </Col>
            <Col>
              <Button color="primary" onClick={this.onSubmit.bind(this)}>
                Fight!
              </Button>
            </Col>
            <Col>
              {battle && <h2>BATTLE IN PROGRESS!</h2>}
            </Col>

          </Row>
        )}
      </Container>
    )
  }
}

export default withContext(Fight)
