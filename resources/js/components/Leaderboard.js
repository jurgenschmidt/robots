import React from 'react'
import { Row, Table } from 'reactstrap'

class Leaderboard extends React.Component {
  constructor() {
    super()
    this.state = {
      robots: [],
      loading: true
    }
  }

  componentDidMount() {
    axios.get('/api/leaderboard').then(response => {
      this.setState({
        robots: response.data,
        loading: false
      })
    })
  }

  render() {
    const { robots, loading } = this.state

    return (
      <Row>
        {loading ? (
          <h3>Loading...</h3>
        ) : (
          <Table striped>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Fights</th>
                <th>Wins</th>
                <th>Losses</th>
              </tr>
            </thead>
            <tbody>
              {robots.map((robot, index) => (
                <tr key={index}>
                  <th>{index+1}</th>
                  <td>{robot.name}</td>
                  <td>{robot.wins + robot.losses}</td>
                  <td>{robot.wins}</td>
                  <td>{robot.losses}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      </Row>
    )
  }
}

export default Leaderboard
