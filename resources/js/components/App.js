import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import AppProvider from '../context/AppProvider'
import Header from './Header'
import Homepage from './Homepage'
import MyRobots from './robots/MyRobots'
import NewRobot from './robots/NewRobot'
import EditRobot from './robots/EditRobot'
import Fight from './Fight'
import Register from './auth/Register'
import Login from './auth/Login'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <AppProvider>
          <Header />
          <Switch>
            <Route exact path="/" component={Homepage} />
            <Route path='/my-robots' component={MyRobots} />
            <Route path='/create' component={NewRobot} />
            <Route path='/edit/:id' component={EditRobot} />
            <Route path='/fight' component={Fight} />
            <Route path='/register' component={Register} />
            <Route path='/login' component={Login} />
          </Switch>
        </AppProvider>
      </BrowserRouter>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('app'))
