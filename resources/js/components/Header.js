import React from 'react'
import { Link } from 'react-router-dom'
import { Navbar, NavbarBrand, Nav, NavItem } from 'reactstrap'

import { withContext } from '../context/AppProvider'

const Header = ({ context }) => {
  return (
    <Navbar color="light" light expand="md">
      <NavbarBrand tag="div">
        <Link to="/" className="nav-link">
          Robots
        </Link>
      </NavbarBrand>
      <Nav className="ml-auto" navbar>
        {context.isAuthenticated ? (
          <>
            <NavItem>
              <Link to="/my-robots" className="nav-link">
                My Robots
              </Link>
            </NavItem>
            <NavItem>
              <Link to="/fight" className="nav-link">
                Fight
              </Link>
            </NavItem>
            <NavItem>
              <Link to="/" className="nav-link" onClick={() => context.logout()}>
                Logout
              </Link>
            </NavItem>
          </>
        ) : (
          <>
            <NavItem>
              <Link to="/register" className="nav-link">
                Register
              </Link>
            </NavItem>
            <NavItem>
              <Link to="/login" className="nav-link">
                Login
              </Link>
            </NavItem>
          </>
        )}
      </Nav>
    </Navbar>
  )
}

export default withContext(Header)
