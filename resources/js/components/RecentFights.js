import React from 'react'
import { Row, Table } from 'reactstrap'

class RecentFights extends React.Component {
  constructor() {
    super()
    this.state = {
      battles: [],
      loading: true
    }
  }

  componentDidMount() {
    axios.get('/api/recent').then(response => {
      this.setState({
        battles: response.data,
        loading: false
      })
    })
  }

  render() {
    const { battles, loading } = this.state

    return (
      <Row>
        {loading ? (
          <h3>Loading...</h3>
        ) : (
          <Table striped>
            <thead>
              <tr>
                <th>#</th>
                <th>Attacker</th>
                <th>Defender</th>
                <th>Winner</th>
              </tr>
            </thead>
            <tbody>
              {battles.map((battle, index) => (
                <tr key={index}>
                  <th>{index+1}</th>
                  <td>{battle.attacker.name}</td>
                  <td>{battle.defender.name}</td>
                  <td>{battle.winner.name}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      </Row>
    )
  }
}

export default RecentFights
