import React from 'react'
import { Container } from 'reactstrap'

import RecentFights from './RecentFights'
import Leaderboard from './Leaderboard'

const Homepage = () => {
  return (
    <Container>
      <h2>Latest 5 Fighting Results</h2>
      <RecentFights />
      <h2>Top 10 Robots</h2>
      <Leaderboard />
    </Container>
  )
}

export default Homepage
