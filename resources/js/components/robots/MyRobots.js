import React from 'react'
import { Link } from 'react-router-dom'
import {
  Container,
  Row,
  Col,
  Button,
  Table,
  UncontrolledAlert
} from 'reactstrap'
import Cookies from 'js-cookie'

import { withContext } from '../../context/AppProvider'

class MyRobots extends React.Component {
  constructor() {
    super()
    this.state = {
      robots: [],
      loading: true,
      error: ''
    }
  }

  componentDidMount() {
    this.getRobotsList()
  }

  getRobotsList() {
    axios
      .get('/api/robots/user', {
        headers: {
          Authorization: 'Bearer ' + Cookies.get('token')
        }
      })
      .then(response => {
        this.setState({
          robots: response.data,
          loading: false
        })
      })
  }

  deleteRobot(id) {
    this.setState({ loading: true })
    axios
      .delete(`/api/robots/${id}`, {
        headers: {
          Authorization: 'Bearer ' + Cookies.get('token')
        }
      })
      .then(() => this.getRobotsList())
      .catch(err =>
        this.setState({ error: err.response.data.errors, loading: false })
      )
  }

  render() {
    const { error, robots, loading } = this.state
    const {
      context: { isAuthenticated }
    } = this.props

    if (!isAuthenticated) return <h1>You're not Authenticated.</h1>

    return (
      <Container>
        <Row>
          <Col>
            <h2>My Robots</h2>
          </Col>
          <Col>
            <div className="float-right">
              <Link to="/create">
                <Button color="success">Add</Button>
              </Link>{' '}
              <Button>Import</Button>
            </div>
          </Col>
        </Row>
        <Row>
          {error.length >= 1 && (
            <UncontrolledAlert color="danger">
              {error}
            </UncontrolledAlert>
          )}
          {loading ? (
            <h3>Loading...</h3>
          ) : (
            <Table striped>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Weight</th>
                  <th>Power</th>
                  <th>Speed</th>
                  <th />
                  <th />
                </tr>
              </thead>
              <tbody>
                {robots.map((robot, index) => (
                  <tr key={index}>
                    <th>{index + 1}</th>
                    <td>{robot.name}</td>
                    <td>{robot.weight}</td>
                    <td>{robot.power}</td>
                    <td>{robot.speed}</td>
                    <td>
                      <Link to={`/edit/${robot.id}`}>
                        <Button color="primary">Edit</Button>
                      </Link>
                    </td>
                    <td>
                      <Button
                        color="danger"
                        onClick={e => this.deleteRobot(robot.id, e)}
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          )}
        </Row>
      </Container>
    )
  }
}

export default withContext(MyRobots)
