import React from 'react'
import Cookies from 'js-cookie'
import {
  Container,
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  UncontrolledAlert
} from 'reactstrap'

import { withContext } from '../../context/AppProvider'

class EditRobot extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      loading: true,
      error: ''
    }
  }

  componentDidMount() {
    const robot = this.props.match.params.id

    axios
      .get(`/api/robots/${robot}`, {
        headers: {
          Authorization: 'Bearer ' + Cookies.get('token')
        }
      })
      .then(response => {
        this.setState({ data: response.data, loading: false })
      })
  }

  onChange(propertyName, event) {
    const { data } = this.state
    data[propertyName] = event.target.value
    this.setState({ data })
  }

  onSubmit() {
    const { history } = this.props
    const { data } = this.state
    const robot = this.props.match.params.id

    this.setState({ loading: true })

    axios
      .put(`/api/robots/${robot}`, data, {
        headers: {
          Authorization: 'Bearer ' + Cookies.get('token')
        }
      })
      .then(() => {
        history.push('/my-robots')
      })
      .catch(error => {
        this.setState({ error: error.response.data.errors, loading: false })
      })
  }

  render() {
    const { error, loading, data } = this.state
    const {
      context: { isAuthenticated }
    } = this.props

    if (!isAuthenticated) return <h1>You're not Authenticated.</h1>

    return (
      <Container>
        <Row>
          <Col sm="12" md={{ size: 5, offset: 3 }}>
            {error.length >= 1 && (
              <UncontrolledAlert color="danger">
                <ul>
                  {error.map((er, i) => (
                    <li key={i}>{er}</li>
                  ))}
                </ul>
              </UncontrolledAlert>
            )}
            {loading && <div className="notification">Loading...</div>}
            <Form>
              <FormGroup>
                <Label>Name:</Label>
                <Input
                  type="text"
                  name="name"
                  value={data.name}
                  onChange={this.onChange.bind(this, 'name')}
                />
              </FormGroup>
              <FormGroup>
                <Label>Weight:</Label>
                <Input
                  type="text"
                  name="weight"
                  value={data.weight}
                  onChange={this.onChange.bind(this, 'weight')}
                />
              </FormGroup>
              <FormGroup>
                <Label>Power:</Label>
                <Input
                  type="text"
                  name="power"
                  value={data.power}
                  onChange={this.onChange.bind(this, 'power')}
                />
              </FormGroup>
              <FormGroup>
                <Label>Speed:</Label>
                <Input
                  type="text"
                  name="speed"
                  value={data.speed}
                  onChange={this.onChange.bind(this, 'speed')}
                />
              </FormGroup>
              <FormGroup>
                <Button color="primary" onClick={this.onSubmit.bind(this)}>
                  Submit
                </Button>
              </FormGroup>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }
}
export default withContext(EditRobot)
