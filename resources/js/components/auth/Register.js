import React from 'react'
import {
  Container,
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  UncontrolledAlert
} from 'reactstrap'

import { withContext } from '../../context/AppProvider'

class Register extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: {
        name: '',
        email: '',
        password: '',
        password_confirmation: ''
      },
      loading: false,
      error: ''
    }
  }

  onChange(propertyName, event) {
    const { data } = this.state
    data[propertyName] = event.target.value
    this.setState({ data })
  }

  onSubmit() {
    const { history, context } = this.props
    const { data } = this.state

    this.setState({ loading: true })

    axios
      .post('/api/register', data)
      .then(res => {
        // save token
        context.setToken(res.data.token)
        this.setState({ loading: false })
        history.push('/')
      })
      .catch(error => {
        this.setState({ error: error.response.data, loading: false })
      })
  }

  render() {
    const { error, loading } = this.state
    return (
      <Container>
        <Row>
          <Col sm="12" md={{ size: 5, offset: 3 }}>
            <div className="paper">
              <section className="wrapper">
                {error && (
                  <UncontrolledAlert color="danger">{error}</UncontrolledAlert>
                )}
                {loading && <div className="notification">Loading...</div>}
                <Form>
                  <FormGroup>
                    <Label>Name:</Label>
                    <Input
                      onChange={this.onChange.bind(this, 'name')}
                      type="text"
                      name="name"
                      style={{ height: 50, fontSize: '1.2em' }}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label>Email:</Label>
                    <Input
                      onChange={this.onChange.bind(this, 'email')}
                      type="email"
                      name="email"
                      style={{ height: 50, fontSize: '1.2em' }}
                    />
                  </FormGroup>
                  <FormGroup style={{ marginBottom: 30 }}>
                    <Label>Password:</Label>
                    <Input
                      onChange={this.onChange.bind(this, 'password')}
                      type="password"
                      name="password"
                      style={{ height: 50, fontSize: '1.2em' }}
                    />
                  </FormGroup>
                  <FormGroup style={{ marginBottom: 30 }}>
                    <Label>Password confirmation:</Label>
                    <Input
                      onChange={this.onChange.bind(
                        this,
                        'password_confirmation'
                      )}
                      type="password"
                      name="password_confirmation"
                      style={{ height: 50, fontSize: '1.2em' }}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Button
                      style={{ float: 'right', width: 120 }}
                      color="primary"
                      onClick={this.onSubmit.bind(this)}
                    >
                      Submit
                    </Button>
                  </FormGroup>
                </Form>
              </section>
            </div>
          </Col>
        </Row>
      </Container>
    )
  }
}
export default withContext(Register)
