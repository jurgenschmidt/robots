import React from 'react'
import Cookies from 'js-cookie'

const AppContext = React.createContext()

class AppProvider extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isAuthenticated: !!Cookies.get('user')
    }
    this.setToken = this.setToken.bind(this)
    this.logout = this.logout.bind(this)
  }

  setToken(token) {
    Cookies.set('token', token)
    axios
      .get('/api/user', {
        headers: {
          Authorization: 'Bearer ' + Cookies.get('token')
        }
      })
      .then(res => {
        Cookies.set('user', res.data.name)
        this.setState({ isAuthenticated: !!res.data.name })
      })
      .catch(error => console.log('err', error))
  }

  logout() {
    axios
      .get('/api/logout', {
        headers: {
          Authorization: 'Bearer ' + Cookies.get('token')
        }
      })
      .then(() => {
        Cookies.remove('token')
        Cookies.remove('user')
        this.setState({ isAuthenticated: false })
      })
  }

  render() {
    return (
      <AppContext.Provider
        value={{
          isAuthenticated: this.state.isAuthenticated,
          setToken: this.setToken,
          logout: this.logout
        }}
      >
        {this.props.children}
      </AppContext.Provider>
    )
  }
}

export function withContext(Component) {
  return function ContextComponent(props) {
    return (
      <AppContext.Consumer>
        {context => <Component {...props} context={context} />}
      </AppContext.Consumer>
    )
  }
}

export default AppProvider
