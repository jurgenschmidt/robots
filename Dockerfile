FROM composer:1.8

FROM php:7.2-fpm

# copy composer from previous build stage
COPY --from=composer:1.8 /usr/bin/composer /usr/local/bin/composer

# install php extensions
RUN apt-get update && \
    apt-get install -y unzip git curl libmcrypt-dev mysql-client --no-install-recommends && \
    pecl install mcrypt-1.0.2 && \
    docker-php-ext-enable mcrypt && \
    docker-php-ext-install pdo_mysql

# nvm environment variables
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 11.11.0

# install nvm
# https://github.com/creationix/nvm#install-script
RUN mkdir -p $NVM_DIR
RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash

# install node and npm
RUN echo "source $NVM_DIR/nvm.sh && \
    nvm install $NODE_VERSION && \
    nvm alias default $NODE_VERSION && \
    nvm use default" | bash

# add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

WORKDIR /var/www
